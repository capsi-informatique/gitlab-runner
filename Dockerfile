FROM gitlab/gitlab-runner:v15.8.0
MAINTAINER David Cachau <david.cachau@capsi-informatique.fr>

RUN apt update
RUN apt install -y netcat

ADD runner.sh /runner.sh
RUN chmod +x /runner.sh

ENTRYPOINT ["/runner.sh"]
